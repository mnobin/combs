def issueKey = 'DS-10'

def issue = get("/rest/api/2/issue/${issueKey}")
        .header('Content-Type', 'application/json')
        .asObject(Map)
        .body


def getComments = get('/rest/api/2/issue/DS-10/comment')
 .header('Content-Type', 'application/json')
 .asObject(Map)
 
if(getComments.status == 200)
{
    logger.info "comment exist"
    def comments = getComments.body.comments["body"]
    def lastComment = getComments.body.size
    return lastComment
    def issuelinks = issue.fields["issuelinks"] //Json of inwards & outwards issues
    def inwardIssue = issuelinks["inwardIssue"] //Json of all inward links
    def linkType = issuelinks["type"]["name"] //Json of all links types name.
    def size = linkType.size//Integer - number of links
    
    def remoteKey =  inwardIssue[1]["key"] //stores the linked issue key
    def commentResp = post("/rest/api/2/issue/${remoteKey}/comment")
        .header('Content-Type', 'application/json')
        .body([body: comments[0].toString()])
        .asObject(Map)
}
else
{
    logger.info "comment doesnt exist"   
}
/*//if (getComments.status == 200){
 def comments = getComments.body.comments["body"]
 
 logger.info("comments: "+comments)
}

*/
